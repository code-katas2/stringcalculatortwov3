﻿using System;
using System.Collections.Generic;

namespace StringCalculatorTwoV3
{
    public class StringCalculatorDelimiter : IStringCalculatorDelimiter
    {
        private const char comma = ',';
        private const char newLine = '\n';
        private const char hash = '#';
        private const char leftSquare = '[';
        private const char rightSquare = ']';
        private const char lessThan = '<';
        private const char greaterThan = '>';
        private const string doubleLessThan = "<<";
        private const string doubleGreaterThan = ">>";

        public string[] GetSeparators(string numbers)
        {
            var separatorArray = new string[] { leftSquare.ToString(), rightSquare.ToString() };

            if (numbers.StartsWith(lessThan.ToString()))
            {
                var delimiter = numbers.Substring(0, numbers.IndexOf(hash));
                separatorArray = delimiter.Split(lessThan, greaterThan);
            }

            if (numbers.Contains(doubleLessThan) || numbers.Contains(doubleGreaterThan))
            {
                separatorArray = new string[] { lessThan.ToString(), greaterThan.ToString() };
            }

            return separatorArray;
        }

        public string[] GetDelimiters(string numbers, string[] separatorArray)
        {
            var delimiterArray = new string[] { comma.ToString(), newLine.ToString() };

            if (numbers.StartsWith(hash.ToString()))
            {
                var delimiter = string.Concat(numbers.Split(hash));
                delimiter = delimiter.Substring(0, delimiter.IndexOf(newLine));
                delimiterArray = new string[] { delimiter };
            }

            if (numbers.Contains(leftSquare.ToString()) || numbers.Contains(lessThan.ToString()))
            {
                var hashPostion = numbers.IndexOf(hash) + 2;

                var delimiter = numbers.Substring(hashPostion, numbers.IndexOf(newLine) - hashPostion);

                delimiterArray = delimiter.Split(separatorArray, StringSplitOptions.RemoveEmptyEntries);
            }

            return delimiterArray;
        }

        public List<int> GetNumbers(string numbers, string[] delimiterArray)
        {
            var numbersList = new List<int>();

            if (numbers.Contains(hash.ToString()))
            {
                numbers = numbers.Split(newLine)[1];
            }

            foreach (var number in numbers.Split(delimiterArray, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    var convertedNumbers = int.Parse(number);

                    numbersList.Add(convertedNumbers);
                }
                catch
                {
                    var convertedLetter = (int)number.ToUpper()[0];
                    convertedLetter -= 65;

                    if (convertedLetter <= 9)
                    {
                        numbersList.Add(convertedLetter);
                    }
                }
            }

            return numbersList;
        }


    }
}
