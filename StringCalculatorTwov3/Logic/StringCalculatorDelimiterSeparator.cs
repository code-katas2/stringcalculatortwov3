﻿

namespace StringCalculatorTwoV3
{
    public class StringCalculatorDelimiterSeparator : IStringCalculatorDelimiterSeparator
    {

        private const char hash = '#';
        private const char leftSquare = '[';
        private const char rightSquare = ']';
        private const char lessThan = '<';
        private const char greaterThan = '>';
        private const string doubleLessThan = "<<";
        private const string doubleGreaterThan = ">>";

        public string[] GetSeparators(string numbers)
        {
            var separatorArray = new string[] { leftSquare.ToString(), rightSquare.ToString() };

            if (numbers.StartsWith(lessThan.ToString()))
            {
                var delimiter = numbers.Substring(0, numbers.IndexOf(hash));
                separatorArray = delimiter.Split(lessThan, greaterThan);
            }

            if (numbers.Contains(doubleLessThan) || numbers.Contains(doubleGreaterThan))
            {
                separatorArray = new string[] { lessThan.ToString(), greaterThan.ToString() };
            }

            return separatorArray;
        }

    }
}
