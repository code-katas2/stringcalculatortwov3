﻿

using System.Collections.Generic;

namespace StringCalculatorTwoV3
{
    public class StringCalculator : IStringCalculator
    {
        private IStringCalculatorDelimiter _calculatorDelimiter;
        private IStringCalculatorDelimiterSeparator _calculatorDelimiterSeparator;
       
        private IStringCalculatorExceptionHandler _calculatorExceptionHandler;

        public int Subract(string numbers)
        {
            _calculatorDelimiter = new StringCalculatorDelimiter();
            _calculatorExceptionHandler = new StringCalculatorExceptionHandler();
            _calculatorDelimiterSeparator = new StringCalculatorDelimiterSeparator();

            var difference = 0;
            if (string.IsNullOrEmpty(numbers))
            {
                return difference;
            }
           
            var separatorArray = _calculatorDelimiterSeparator.GetSeparators(numbers);
            var delimiterArray =_calculatorDelimiter.GetDelimiters(numbers, separatorArray);
            var numberList = _calculatorDelimiter.GetNumbers(numbers, delimiterArray);
            
            _calculatorExceptionHandler.ThrowExceptionNumbersGreaterThousand(numberList);

            difference = GetDifference(numberList);

            return difference;
        }

        private int GetDifference(List<int> numbersList)
        {
            var difference = 0;

            foreach (var number in numbersList)
            {
                if (number > 0)
                {
                    difference += number * -1;
                }
                else
                {
                    difference += number;
                }
            }

            return difference;
        }
    }
}
