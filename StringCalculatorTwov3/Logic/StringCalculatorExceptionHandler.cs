﻿using System;
using System.Collections.Generic;

namespace StringCalculatorTwoV3
{
    public class StringCalculatorExceptionHandler : IStringCalculatorExceptionHandler
    {
        public void ThrowExceptionNumbersGreaterThousand(List<int> numbersList)
        {
            var numbersGreaterThousand = string.Empty;
            foreach (var number in numbersList)
            {
                if (number > 1000)
                {
                    numbersGreaterThousand = string.Join(" ", numbersGreaterThousand, number);
                }
            }

            if (!string.IsNullOrEmpty(numbersGreaterThousand))
            {
                throw new Exception("Numbers greater than thousand." + numbersGreaterThousand);
            }
        }
    }
}
