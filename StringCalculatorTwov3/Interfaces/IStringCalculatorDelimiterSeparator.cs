﻿namespace StringCalculatorTwoV3
{
    public interface IStringCalculatorDelimiterSeparator
    {
        string[] GetSeparators(string numbers);
    }
}