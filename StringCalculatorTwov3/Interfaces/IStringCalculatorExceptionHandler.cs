﻿using System.Collections.Generic;

namespace StringCalculatorTwoV3
{
    public interface IStringCalculatorExceptionHandler
    {
        void ThrowExceptionNumbersGreaterThousand(List<int> numbersList);
    }
}