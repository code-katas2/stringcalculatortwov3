﻿using System.Collections.Generic;

namespace StringCalculatorTwoV3
{
    public interface IStringCalculatorDelimiter
    {
        string[] GetDelimiters(string numbers, string[] separatorArray);
        List<int> GetNumbers(string numbers, string[] delimiterArray);
        string[] GetSeparators(string numbers);
    }
}