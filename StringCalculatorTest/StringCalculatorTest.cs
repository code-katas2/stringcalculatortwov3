﻿using System;
using NUnit.Framework;
using StringCalculatorTwoV3;
namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private IStringCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new StringCalculator();
        }

        [Test]
        public void GivenEmptyString_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Subract("");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenOneNumber_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -1;

            //Act
            var actual = _calculator.Subract("1");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenTwoNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -3;

            //Act
            var actual = _calculator.Subract("1,2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenUnlimitedNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("1,2,3");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenNewLineAsDelimiter_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("1\n2,3");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenCustomDelimiter_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -3;

            //Act
            var actual = _calculator.Subract("##;\n1;2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenNegativeNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -12;

            //Act
            var actual = _calculator.Subract("##;\n10;-2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
        
        [Test]
        public void GivenNumbersGreaterThanThousand_WhenSubtracting_ThenThrowException()
        {
            //Arrange
            var expected = "Numbers greater than thousand. 1001 2000";

            //Act
            var exception = Assert.Throws<Exception>(()=>_calculator.Subract("##;\n1;1001;2000"));

            //Assert
            Assert.AreEqual(expected,exception.Message);
        }

        [Test]
        public void GivenAnyLengthDelimiter_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("##[***]\n1***2***3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenMultipleDelimiters_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("##[*][%]\n1*2%3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
       
        [Test]
        public void GivenAnyLengthMultipleDelimiters_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("##[*#$][%$$]\n1*#$2%$$3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenOneLettersAsNumber_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Subract("a");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenTwoLettersAsNumbers_WhenSubtracting_ThenReturnDifference()
        {
            //Arrange
            var expected = -1;

            //Act
            var actual = _calculator.Subract("a,b");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenInvalidLetterAsNumber_WhenSubtracting_ThenIgnoreLetter()
        {
            //Arrange
            var expected = -17;

            //Act
            var actual = _calculator.Subract("i,j,k");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenDelimiterSeparators_WhenSubtracting_ThenReturnSum()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("<(>)##(*)\n1*2*3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenUnlimitedNumbersWithDelimiterSeparators_WhenSubtracting_ThenReturnSum()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("<{>}##{*}\n1*2*3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenMultipleDelimiterSeparators_WhenSubtracting_ThenReturnSum()
        {
            //Arrange
            var expected = -6;

            //Act
            var actual = _calculator.Subract("<[>}##[::}\n1::2::3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenFlagsAsDelimiterSeparators_WhenSubtracting_ThenReturnSum()
        {
            //Arrange
            var expected = -15;

            //Act
            var actual = _calculator.Subract("<>><##>&<\n4&5&6");

            //Assert
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void GivenFlagsAsMultipleDelimitersSeparator_WhenSubtracting_ThenReturnSum()
        {
            //Arrange
            var expected = -18;

            //Act
            var actual = _calculator.Subract("<<>>##<$$$><###>\n5$$$6###7");

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
